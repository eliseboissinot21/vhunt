# Installer VHunt

VHunt est un **jeu mobile disponible sur Android uniquement**.
Si vous ne disposez pas d'un téléphone adapté, vous pourrez toujours y jouer sur PC via un **émulateur**. 
Pour tous les cas évoqués, suivez ce guide.

[[_TOC_]]

## Installation sur PC

### Téléchargez l'application

Quelque soit votre plateforme de jeu, vous aurez besoin de télécharger le .apk correspondant à partir de [ce lien](https://lttuna.itch.io/vhunt).

### Installez un émulateur

Pour les joueurs de PC, vous aurez besoin d'un émulateur afin de simuler l'utilisation d'un téléphone (par exemple [Bluestacks](https://cloud.bluestacks.com/api/getdownloadnow?platform=win&win_version=10&mac_version=&client_uuid=449e38b2-832d-403e-a1b8-05e14fdc8407&app_pkg=&platform_cloud=%257B%2522description%2522%253A%2522Chrome%2520100.0.4896.127%2520on%2520Windows%252010%252064-bit%2522%252C%2522layout%2522%253A%2522Blink%2522%252C%2522manufacturer%2522%253Anull%252C%2522name%2522%253A%2522Chrome%2522%252C%2522prerelease%2522%253Anull%252C%2522product%2522%253Anull%252C%2522ua%2522%253A%2522Mozilla%252F5.0%2520(Windows%2520NT%252010.0%253B%2520Win64%253B%2520x64)%2520AppleWebKit%252F537.36%2520(KHTML%252C%2520like%2520Gecko)%2520Chrome%252F100.0.4896.127%2520Safari%252F537.36%2522%252C%2522version%2522%253A%2522100.0.4896.127%2522%252C%2522os%2522%253A%257B%2522architecture%2522%253A64%252C%2522family%2522%253A%2522Windows%2522%252C%2522version%2522%253A%252210%2522%257D%257D&preferred_lang=fr&utm_source=&utm_medium=&gaCookie=GA1.1.1962036235.1650821902&gclid=&clickid=&msclkid=&affiliateId=&offerId=&transaction_id=&aff_sub=&first_landing_page=https%253A%252F%252Fwww.bluestacks.com%252Ffr%252Findex.html&referrer=https%253A%252F%252Fwww.google.com%252F&download_page_referrer=&utm_campaign=homepage-dl-button-fr&exit_utm_campaign=bsx-install-button-home-fr&incompatible=false&bluestacks_version=bs5)). On fera avec BlueStacks sur ce guide.

Installez ce dernier, lancez le, et sélectionnez l'icone en bas à gauche. 

![icone](1.PNG)

Il vous sera peut-être demandé de redémarrer le PC. Acceptez et redémarrez.

Relancez Bluestacks si ce dernier a été fermé. Cliquez de nouveau sur le logo, puis sur la barre latérale droite, sélectionnez le menu `APK`.

![menu APK](2.PNG)

Sélectionnez ensuite le .apk préalablement installé dans l'explorateur qui vient de s'ouvrir.
Votre jeu est maintenant installé !

![VHunt](3.PNG)

## Installation sur Android

### Téléchargez l'application

Téléchargez directement l'application depuis **votre téléphone** via [ce lien](https://lttuna.itch.io/vhunt).

Après le téléchargement, une erreur de sécurité vous demandera de confirmer le téléchargement à partir de sources inconnus. Suivez l'instruction de votre mobile pour autoriser son installation.

